﻿
<%@ page language="C#" autoeventwireup="true" inherits="老师信息管理_Deleteteacher, App_Web_deleteteacher.aspx.3efd1698" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
<div>
    <asp:DropDownList ID="DropDownList1" runat="server" AutoPostBack="True" DataSourceID="ods院系" DataTextField="院系名称" DataValueField="院系ID"></asp:DropDownList>
        <asp:GridView ID="GridView2" runat="server" AutoGenerateColumns="False" DataKeyNames="教师工号" DataSourceID="ods老师信息" CellPadding="4" ForeColor="#333333" GridLines="None" Height="169px" Width="950px">
            <AlternatingRowStyle BackColor="White" />
            <Columns>
                <asp:BoundField DataField="教师工号" HeaderText="教师工号" ReadOnly="True" SortExpression="教师工号" />
                <asp:BoundField DataField="教师姓名" HeaderText="教师姓名" SortExpression="教师姓名" />
                <asp:BoundField DataField="政治面貌名称" HeaderText="政治面貌名称" SortExpression="政治面貌名称" />
                <asp:BoundField DataField="职称名称" HeaderText="职称名称" SortExpression="职称名称" />
                <asp:BoundField DataField="院系名称" HeaderText="院系名称" SortExpression="院系名称" />
                <asp:TemplateField ShowHeader="False">
                    <ItemTemplate>
                        <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="False" CommandName="Delete" Text="删除" OnClientClick="return confirm('确认删除吗？');"></asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <EditRowStyle BackColor="#7C6F57" />
            <FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
            <HeaderStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#666666" ForeColor="White" HorizontalAlign="Center" />
            <RowStyle BackColor="#E3EAEB" />
            <SelectedRowStyle BackColor="#C5BBAF" Font-Bold="True" ForeColor="#333333" />
            <SortedAscendingCellStyle BackColor="#F8FAFA" />
            <SortedAscendingHeaderStyle BackColor="#246B61" />
            <SortedDescendingCellStyle BackColor="#D4DFE1" />
            <SortedDescendingHeaderStyle BackColor="#15524A" />
        </asp:GridView>
        <asp:ObjectDataSource ID="ods老师信息" runat="server" DeleteMethod="DeleteQuery" OldValuesParameterFormatString="{0}" SelectMethod="老师信息" TypeName="MainDataSetTableAdapters.老师信息TableAdapter">
            <DeleteParameters>
                <asp:Parameter Name="教师工号" Type="String" />
            </DeleteParameters>
            <SelectParameters>
                <asp:ControlParameter ControlID="DropDownList1" Name="院系ID" PropertyName="SelectedValue" Type="String" />
            </SelectParameters>
    </asp:ObjectDataSource>
    </div>
        <asp:ObjectDataSource ID="ods院系" runat="server" InsertMethod="Insert" OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" TypeName="MainDataSetTableAdapters.院系TableAdapter">
            <InsertParameters>
                <asp:Parameter Name="院系ID" Type="String" />
                <asp:Parameter Name="院系名称" Type="String" />
                <asp:Parameter Name="院系序号" Type="Int16" />
                <asp:Parameter Name="是否执行" Type="String" />
                <asp:Parameter Name="备注" Type="String" />
                <asp:Parameter Name="备注1" Type="String" />
                <asp:Parameter Name="备注2" Type="String" />
                <asp:Parameter Name="备注3" Type="String" />
                <asp:Parameter Name="备注4" Type="String" />
                <asp:Parameter Name="备注5" Type="String" />
            </InsertParameters>
        </asp:ObjectDataSource>
    </form>
</body>
</html>
