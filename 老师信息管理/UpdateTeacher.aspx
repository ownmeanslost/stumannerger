﻿<%@ page language="C#" autoeventwireup="true" inherits="老师信息管理_UpdateTeacher, App_Web_updateteacher.aspx.3efd1698" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:DropDownList ID="DropDownList1" runat="server" AutoPostBack="True" DataSourceID="ods院系" DataTextField="院系名称" DataValueField="院系ID"></asp:DropDownList>
        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataKeyNames="教师工号" DataSourceID="ods老师信息" OnRowUpdating="GridView1_RowUpdating1" EnablePersistedSelection="True" EnableViewState="False" CellPadding="4" ForeColor="#333333" GridLines="None" style="margin-right: 0px" Height="169px" Width="950px">
            <AlternatingRowStyle BackColor="White" />
            <Columns>
                <asp:BoundField DataField="教师工号" HeaderText="教师工号" ReadOnly="True" SortExpression="教师工号"/>
                <asp:TemplateField HeaderText="教师姓名" SortExpression="教师姓名">
                    <EditItemTemplate>
                        <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("教师姓名") %>'></asp:TextBox>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label5" runat="server" Text='<%# Bind("教师姓名") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="教师性别" SortExpression="教师性别">
                    <EditItemTemplate>
                        <asp:DropDownList ID="DropDownList2" runat="server">
                            <asp:ListItem>男</asp:ListItem>
                            <asp:ListItem>女</asp:ListItem>
                        </asp:DropDownList>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label1" runat="server" Text='<%# Bind("教师性别") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="出生年月" SortExpression="出生年月">
                    <EditItemTemplate>
                        <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("出生年月","{0:yyyy-MM-dd}") %>'></asp:TextBox>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label6" runat="server" Text='<%# Bind("出生年月","{0:yyyy-MM-dd}") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="院系名称" SortExpression="院系名称">
                    <EditItemTemplate>
                        <asp:DropDownList ID="DropDownList3" runat="server" DataSourceID="ods院系" DataTextField="院系名称" DataValueField="院系ID">
                        </asp:DropDownList>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label2" runat="server" Text='<%# Bind("院系名称") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="政治面貌名称" SortExpression="政治面貌名称">
                    <EditItemTemplate>
                        <asp:DropDownList ID="DropDownList4" runat="server" DataSourceID="ods政治面貌" DataTextField="政治面貌名称" DataValueField="政治面貌ID">
                        </asp:DropDownList>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label3" runat="server" Text='<%# Bind("政治面貌名称") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="职称名称" SortExpression="职称名称">
                    <EditItemTemplate>
                        <asp:DropDownList ID="DropDownList5" runat="server" DataSourceID="ods职称" DataTextField="职称名称" DataValueField="职称ID">
                        </asp:DropDownList>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label4" runat="server" Text='<%# Bind("职称名称") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField ShowHeader="False">
                    <EditItemTemplate>
                        <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="True" CommandName="Update" Text="更新" OnClick="LinkButton1_Click"></asp:LinkButton>
                        &nbsp;<asp:LinkButton ID="LinkButton2" runat="server" CausesValidation="False" CommandName="Cancel" Text="取消"></asp:LinkButton>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="False" CommandName="Edit" Text="编辑"></asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <EditRowStyle BackColor="#7C6F57" />
            <FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
            <HeaderStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#666666" ForeColor="White" HorizontalAlign="Center" />
            <RowStyle BackColor="#E3EAEB" />
            <SelectedRowStyle BackColor="#C5BBAF" Font-Bold="True" ForeColor="#333333" />
            <SortedAscendingCellStyle BackColor="#F8FAFA" />
            <SortedAscendingHeaderStyle BackColor="#246B61" />
            <SortedDescendingCellStyle BackColor="#D4DFE1" />
            <SortedDescendingHeaderStyle BackColor="#15524A" />
        </asp:GridView>
        <br />
        <br />
        <asp:ObjectDataSource ID="ods院系" runat="server" InsertMethod="Insert" OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" TypeName="MainDataSetTableAdapters.院系TableAdapter">
            <InsertParameters>
                <asp:Parameter Name="院系ID" Type="String" />
                <asp:Parameter Name="院系名称" Type="String" />
                <asp:Parameter Name="院系序号" Type="Int16" />
                <asp:Parameter Name="是否执行" Type="String" />
                <asp:Parameter Name="备注" Type="String" />
                <asp:Parameter Name="备注1" Type="String" />
                <asp:Parameter Name="备注2" Type="String" />
                <asp:Parameter Name="备注3" Type="String" />
                <asp:Parameter Name="备注4" Type="String" />
                <asp:Parameter Name="备注5" Type="String" />
            </InsertParameters>
        </asp:ObjectDataSource>
        <asp:ObjectDataSource ID="ods老师信息" runat="server" OldValuesParameterFormatString="original_{0}" SelectMethod="老师信息" TypeName="MainDataSetTableAdapters.老师信息TableAdapter" UpdateMethod="UpdateQuery" ConflictDetection="CompareAllValues">
            <SelectParameters>
                <asp:ControlParameter ControlID="DropDownList1" Name="院系ID" PropertyName="SelectedValue" Type="String" />
            </SelectParameters>
            <UpdateParameters>
                <asp:Parameter Name="教师姓名" Type="String" />
                <asp:Parameter Name="教师性别" Type="String" />
                <asp:Parameter Name="教师政治面貌ID" Type="String" />
                <asp:Parameter Name="出生年月" Type="String" />
                <asp:Parameter Name="教师职称ID" Type="String" />
                <asp:Parameter Name="教师院系ID" Type="String" />
                <asp:Parameter Name="教师工号" Type="String" />
            </UpdateParameters>
        </asp:ObjectDataSource>
        <asp:ObjectDataSource ID="ods职称" runat="server" DeleteMethod="Delete" InsertMethod="Insert" OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" TypeName="MainDataSetTableAdapters.职称TableAdapter" UpdateMethod="Update">
            <DeleteParameters>
                <asp:Parameter Name="Original_职称ID" Type="String" />
            </DeleteParameters>
            <InsertParameters>
                <asp:Parameter Name="职称ID" Type="String" />
                <asp:Parameter Name="职称名称" Type="String" />
                <asp:Parameter Name="职称序号" Type="Int16" />
                <asp:Parameter Name="是否执行" Type="String" />
                <asp:Parameter Name="备注" Type="String" />
                <asp:Parameter Name="备注1" Type="String" />
                <asp:Parameter Name="备注2" Type="String" />
                <asp:Parameter Name="备注3" Type="String" />
                <asp:Parameter Name="备注4" Type="String" />
                <asp:Parameter Name="备注5" Type="String" />
            </InsertParameters>
            <UpdateParameters>
                <asp:Parameter Name="职称名称" Type="String" />
                <asp:Parameter Name="职称序号" Type="Int16" />
                <asp:Parameter Name="是否执行" Type="String" />
                <asp:Parameter Name="备注" Type="String" />
                <asp:Parameter Name="备注1" Type="String" />
                <asp:Parameter Name="备注2" Type="String" />
                <asp:Parameter Name="备注3" Type="String" />
                <asp:Parameter Name="备注4" Type="String" />
                <asp:Parameter Name="备注5" Type="String" />
                <asp:Parameter Name="Original_职称ID" Type="String" />
            </UpdateParameters>
        </asp:ObjectDataSource>
        <br />
        <asp:ObjectDataSource ID="ods政治面貌" runat="server" DeleteMethod="Delete" InsertMethod="Insert" OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" TypeName="MainDataSetTableAdapters.政治面貌TableAdapter" UpdateMethod="Update">
            <DeleteParameters>
                <asp:Parameter Name="Original_政治面貌ID" Type="String" />
            </DeleteParameters>
            <InsertParameters>
                <asp:Parameter Name="政治面貌ID" Type="String" />
                <asp:Parameter Name="政治面貌名称" Type="String" />
                <asp:Parameter Name="政治面貌序号" Type="Int16" />
                <asp:Parameter Name="是否执行" Type="String" />
                <asp:Parameter Name="备注" Type="String" />
                <asp:Parameter Name="备注1" Type="String" />
                <asp:Parameter Name="备注2" Type="String" />
                <asp:Parameter Name="备注3" Type="String" />
                <asp:Parameter Name="备注4" Type="String" />
                <asp:Parameter Name="备注5" Type="String" />
            </InsertParameters>
            <UpdateParameters>
                <asp:Parameter Name="政治面貌名称" Type="String" />
                <asp:Parameter Name="政治面貌序号" Type="Int16" />
                <asp:Parameter Name="是否执行" Type="String" />
                <asp:Parameter Name="备注" Type="String" />
                <asp:Parameter Name="备注1" Type="String" />
                <asp:Parameter Name="备注2" Type="String" />
                <asp:Parameter Name="备注3" Type="String" />
                <asp:Parameter Name="备注4" Type="String" />
                <asp:Parameter Name="备注5" Type="String" />
                <asp:Parameter Name="Original_政治面貌ID" Type="String" />
            </UpdateParameters>
        </asp:ObjectDataSource>
        <br />
    </div>
    </form>
</body>
</html>
