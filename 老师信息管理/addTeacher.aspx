﻿<%@ page language="C#" autoeventwireup="true" inherits="老师信息管理_Default, App_Web_addteacher.aspx.3efd1698" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div style="vertical-align: middle; text-align: center;">
        <table style="border-style: ridge; border-color: inherit; border-width: medium; height: 435px; width: 470px;margin-left:auto;margin-right:auto" border="1">
            <tr>
                <td><asp:Label ID="Label1" runat="server" Text="老师工号:"></asp:Label></td>
                <td><asp:TextBox ID="TextBox1" runat="server" style="margin-bottom: 0px"></asp:TextBox>
                    <br />
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="TextBox1" ErrorMessage="账号输入不规范" Font-Bold="True" ForeColor="Red" ValidationExpression="[0-9]{10}"></asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr>
                <td><asp:Label ID="Label2" runat="server" Text="老师姓名:"></asp:Label></td>
                <td><asp:TextBox ID="TextBox2" runat="server"></asp:TextBox>
                    <br />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="TextBox2" ErrorMessage="名字不能为空" Font-Bold="True" ForeColor="Red"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td><asp:Label ID="Label3" runat="server" Text="老师性别:"></asp:Label></td>
                <td><asp:DropDownList ID="DropDownList1" runat="server" Height="20px" Width="146px">
                    <asp:ListItem>男</asp:ListItem>
                    <asp:ListItem>女</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td><asp:Label ID="Label5" runat="server" Text="政治面貌:"></asp:Label></td>
                <td>
                    <asp:DropDownList ID="DropDownList3" runat="server" Height="20px" Width="146px" DataSourceID="ods政治面貌" DataTextField="政治面貌名称" DataValueField="政治面貌ID">
                    </asp:DropDownList></td>
            </tr>
            <tr>
                <td><asp:Label ID="Label6" runat="server" Text="出生年月:"></asp:Label></td>
                <td>
                    <asp:TextBox ID="TextBox3" runat="server"></asp:TextBox>
                    <br />
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="TextBox3" ErrorMessage="输入不规范格式为'yyyy-mm-dd'" Font-Bold="True" ForeColor="Red" ValidationExpression="\d{4}-\d{2}-\d{2}"></asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr>
                <td><asp:Label ID="Label7" runat="server" Text="老师职称:"></asp:Label></td>
                <td>
                    <asp:DropDownList ID="DropDownList4" runat="server" Height="20px" Width="146px" DataSourceID="ods职称" DataTextField="职称名称" DataValueField="职称ID">
                    </asp:DropDownList></td>
            </tr>
            <tr>
                <td><asp:Label ID="Label8" runat="server" Text="所属院系:"></asp:Label></td>
                <td>
                    <asp:DropDownList ID="DropDownList5" runat="server" Height="20px" Width="146px" DataSourceID="ods院系" DataTextField="院系名称" DataValueField="院系ID">
                    </asp:DropDownList></td>
            </tr>
            <tr>
                <td colspan="2"><asp:Button ID="Button1" runat="server" Text="提交" Height="35px" OnClick="Button1_Click" Width="106px" /></td>    
            </tr>
           
        </table>
        
       
        <asp:ObjectDataSource ID="ods政治面貌" runat="server" DeleteMethod="Delete" InsertMethod="Insert" OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" TypeName="MainDataSetTableAdapters.政治面貌TableAdapter" UpdateMethod="Update">
            <DeleteParameters>
                <asp:Parameter Name="Original_政治面貌ID" Type="String" />
            </DeleteParameters>
            <InsertParameters>
                <asp:Parameter Name="政治面貌ID" Type="String" />
                <asp:Parameter Name="政治面貌名称" Type="String" />
                <asp:Parameter Name="政治面貌序号" Type="Int16" />
                <asp:Parameter Name="是否执行" Type="String" />
                <asp:Parameter Name="备注" Type="String" />
                <asp:Parameter Name="备注1" Type="String" />
                <asp:Parameter Name="备注2" Type="String" />
                <asp:Parameter Name="备注3" Type="String" />
                <asp:Parameter Name="备注4" Type="String" />
                <asp:Parameter Name="备注5" Type="String" />
            </InsertParameters>
            <UpdateParameters>
                <asp:Parameter Name="政治面貌名称" Type="String" />
                <asp:Parameter Name="政治面貌序号" Type="Int16" />
                <asp:Parameter Name="是否执行" Type="String" />
                <asp:Parameter Name="备注" Type="String" />
                <asp:Parameter Name="备注1" Type="String" />
                <asp:Parameter Name="备注2" Type="String" />
                <asp:Parameter Name="备注3" Type="String" />
                <asp:Parameter Name="备注4" Type="String" />
                <asp:Parameter Name="备注5" Type="String" />
                <asp:Parameter Name="Original_政治面貌ID" Type="String" />
            </UpdateParameters>
        </asp:ObjectDataSource>
        <asp:ObjectDataSource ID="ods职称" runat="server" DeleteMethod="Delete" InsertMethod="Insert" OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" TypeName="MainDataSetTableAdapters.职称TableAdapter" UpdateMethod="Update">
            <DeleteParameters>
                <asp:Parameter Name="Original_职称ID" Type="String" />
            </DeleteParameters>
            <InsertParameters>
                <asp:Parameter Name="职称ID" Type="String" />
                <asp:Parameter Name="职称名称" Type="String" />
                <asp:Parameter Name="职称序号" Type="Int16" />
                <asp:Parameter Name="是否执行" Type="String" />
                <asp:Parameter Name="备注" Type="String" />
                <asp:Parameter Name="备注1" Type="String" />
                <asp:Parameter Name="备注2" Type="String" />
                <asp:Parameter Name="备注3" Type="String" />
                <asp:Parameter Name="备注4" Type="String" />
                <asp:Parameter Name="备注5" Type="String" />
            </InsertParameters>
            <UpdateParameters>
                <asp:Parameter Name="职称名称" Type="String" />
                <asp:Parameter Name="职称序号" Type="Int16" />
                <asp:Parameter Name="是否执行" Type="String" />
                <asp:Parameter Name="备注" Type="String" />
                <asp:Parameter Name="备注1" Type="String" />
                <asp:Parameter Name="备注2" Type="String" />
                <asp:Parameter Name="备注3" Type="String" />
                <asp:Parameter Name="备注4" Type="String" />
                <asp:Parameter Name="备注5" Type="String" />
                <asp:Parameter Name="Original_职称ID" Type="String" />
            </UpdateParameters>
        </asp:ObjectDataSource>
        <asp:ObjectDataSource ID="ods院系" runat="server" InsertMethod="Insert" OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" TypeName="MainDataSetTableAdapters.院系TableAdapter">
            <InsertParameters>
                <asp:Parameter Name="院系ID" Type="String" />
                <asp:Parameter Name="院系名称" Type="String" />
                <asp:Parameter Name="院系序号" Type="Int16" />
                <asp:Parameter Name="是否执行" Type="String" />
                <asp:Parameter Name="备注" Type="String" />
                <asp:Parameter Name="备注1" Type="String" />
                <asp:Parameter Name="备注2" Type="String" />
                <asp:Parameter Name="备注3" Type="String" />
                <asp:Parameter Name="备注4" Type="String" />
                <asp:Parameter Name="备注5" Type="String" />
            </InsertParameters>
        </asp:ObjectDataSource>
        
       
    </div>
    </form>
</body>
</html>
